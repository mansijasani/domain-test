import React, { useState } from "react";
import { Form, Button, Card, Grid, Image } from "semantic-ui-react";
import { useForm } from "../componets/forms";
import avatarImage from '../../public/avatar.png';

const DEFAULT_STATE = {
  givenName: "",
  surname: "",
  email: "",
  phone: "",
  house: "",
  street: "",
  suburb: "",
  state: "",
  postcode: "",
  country: "",
};

export default function CardPage() {
  const { state, formField } = useForm(DEFAULT_STATE);
  const [imagePreview, setImagePreview] = useState(avatarImage);

  const handleImageUpload = e => {
    if (e.target.files.length) {
      setImagePreview(URL.createObjectURL(e.target.files[0]))
    }
  }

  return(
    <Grid className="hcard" columns={2} textAlign='left' verticalAlign='middle' stackable>
      <Grid.Column className="hcard--form">
        <h1>hCard Builder</h1>

        <p>PERSONAL DETAILS</p>

        <Form>
          <Form.Group widths="equal">
            <Form.Input
              id="givenName"
              label="GIVEN NAME"
              {...formField("givenName")}
            />
            <Form.Input
              id="surname"
              label="SURNAME"
              {...formField("surname")}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              id="email"
              label="EMAIL"
              type="email"
              {...formField("email")}
            />
            <Form.Input
              id="phone"
              label="PHONE"
              type="phone"
              {...formField("phone")}
            />
          </Form.Group>

          <p>ADDRESS</p>

          <Form.Group widths="equal">
            <Form.Input
              id="house"
              label="HOUSE NAME OR #"
              {...formField("house")}
            />
            <Form.Input
              id="street"
              label="STREET"
              {...formField("street")}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              id="suburb"
              label="SUBURB"
              {...formField("suburb")}
            />
            <Form.Input
              id="state"
              label="STATE"
              {...formField("state")}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              id="postcode"
              label="POSTCODE"
              {...formField("postcode")}
            />
            <Form.Input
              id="country"
              label="COUNTRY"
              {...formField("country")}
            />
          </Form.Group>

          <Grid columns={2}>
            <Grid.Column className="hcard--form-upload-button">
              <label htmlFor="file-upload" className="hcard--form-file-upload">Upload Avatar
                <input id="file-upload" type="file" onChange={handleImageUpload}/>
              </label>
            </Grid.Column>
            <Grid.Column className="hcard--form-create-button">
              <Button primary fluid>Create hCard</Button>
            </Grid.Column>
          </Grid>
        </Form>        
      </Grid.Column>
      
      <Grid.Column className="hacrd--preview">
        <h4>HCARD PREVIEW </h4>
        <Card fluid>
          <Card.Content>
            <Image
              className="hacrd--preview-img"
              floated="right"
              size="tiny"
              src={imagePreview}
              alt="image"
            />
            <Card.Header className="hacrd--preview-header">
              <h2>{state.givenName} {state.surname}</h2>
            </Card.Header>
            <Card.Description>
              <table className="hacrd-preview-data">
                <tbody>
                  <tr>
                    <td>EMAIL</td>
                    <td colSpan="3">{state.email}</td>
                  </tr>
                  <tr>
                    <td>PHONE</td>
                    <td colSpan="3">{state.phone}</td>
                  </tr>
                  <tr>
                    <td>ADDRESS</td>
                    <td colSpan="3">{state.house} {state.street}</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td colSpan="3">{state.suburb}{state.state ? ',': ''} {state.state}</td>
                  </tr>
                  <tr>
                    <td>POSTCODE</td>
                    <td>{state.postcode}</td>
                    <td>COUNTRY</td>
                    <td>{state.country}</td>
                  </tr>
                </tbody>               
              </table>
            </Card.Description>
          </Card.Content>
        </Card>
      </Grid.Column>
    </Grid>
  )
};
