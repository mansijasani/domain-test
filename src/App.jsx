import React, { Component } from 'react';
import './App.css';
import { Container } from 'semantic-ui-react'
import CardPage from './CardPage';

class App extends Component {
  render() {
    return (
      <Container textAlign="center">
        <CardPage />
      </Container>
    );
  }
}

export default App;
