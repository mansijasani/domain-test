import { useState } from "react";

export function useForm(defaultValue) {
  const [state, setState] = useState(defaultValue);

  function formField(field, transform = (i) => i) {
    const valueField = "value";
    
    return {
      [valueField]: state[field],
      onChange: (e, extra) => {
        const { value } = extra || {};
        let newValue = e.target.value || value;

        newValue = transform(newValue);

        setState((st) => ({ ...st, [field]: newValue }));
      },
    };
  }
  return { state, setState, formField };
}